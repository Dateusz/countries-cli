extern crate reqwest;

use reqwest::Error;
use std::io;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let mut current_filters: Vec<&'static str> = Vec::new();

    loop {

        let filters = current_filters.join(",");

        let filters_display = if current_filters.is_empty() {
            "All".to_string()
        } else {
            filters.clone()
        };

        println!("");
        println!("Welcome to Countries CLI, a CLI tool making use of REST Countries API for fetching geopolitical data.");
        println!("");
        println!("Choose your filters:");
        println!("0. Filters: {}", filters_display);
        println!("");
        println!("Type a number to make a choice:");
        println!("1. All available data.");
        println!("2. Search by country name");
        println!("3. Search by currency");
        println!("4. Search by language");
        println!("5. Search by capital city");
        println!("");
        println!("Q. Exit");
        println!("");

        let mut choice = String::new();

        io::stdin()
            .read_line(&mut choice)
            .expect("Failed to read line");

        let choice = choice.trim();

        if choice.to_lowercase() == "q" {
            println!("Goodbye!");
            break;
        }

        match choice.parse::<u32>() {
            Ok(0) => {
                current_filters = choose_filters(&current_filters);
            }
            Ok(1) => {
                fetch_all(&filters).await?;
            }
            Ok(2) => {
                search_by_name(&filters).await?;
            }
            Ok(3) => {
                search_by_currency(&filters).await?;
            }
            Ok(4) => {
                search_by_language(&filters).await?;
            }
            Ok(5) => {
                search_by_capital(&filters).await?;
            }
            _ => {
                println!("Invalid choice.");
            }
        }
    }

    Ok(())
}

async fn fetch_all(filters: &String) -> Result<(), Error> {
    let url = format!("https://restcountries.com/v3.1/all?fields={}", filters);

    format_response(&url).await?;

    Ok(())
}

async fn search_by_name(filters: &String) -> Result<(), Error> {
    let mut country_name = String::new();

    println!("Enter the country name:");
    io::stdin()
        .read_line(&mut country_name)
        .expect("Failed to read line");

    let country_name = country_name.trim();
    let url = format!("https://restcountries.com/v3.1/name/{}?fields={}", country_name, filters);

    format_response(&url).await?;

    Ok(())
}

async fn search_by_currency(filters: &String) -> Result<(), Error> {
    let mut currency = String::new();

    println!("Enter the currency");
    println!("Name (full or partial) or symbol (ie. USD, EUR). Not case-sensitive.");
    io::stdin()
        .read_line(&mut currency)
        .expect("Failed to read line");

    let currency = currency.trim();
    let url = format!("https://restcountries.com/v3.1/currency/{}?fields={}", currency, filters);

    format_response(&url).await?;

    Ok(())
}

async fn search_by_language(filters: &String) -> Result<(), Error> {
    let mut language = String::new();

    println!("Enter the language (full name, i.e. spanish)");
    io::stdin()
        .read_line(&mut language)
        .expect("Failed to read line");

    let currency = language.trim();
    let url = format!("https://restcountries.com/v3.1/lang/{}?fields={}", currency, filters);

    format_response(&url).await?;

    Ok(())
}

async fn search_by_capital(filters: &String) -> Result<(), Error> {
    let mut capital = String::new();

    println!("Enter the capital city name.");
    println!("Name (full or partial). Not case-sensitive.");
    io::stdin()
        .read_line(&mut capital)
        .expect("Failed to read line");

    let currency = capital.trim();
    let url = format!("https://restcountries.com/v3.1/capital/{}?fields={}", currency, filters);

    format_response(&url).await?;

    Ok(())
}

async fn format_response(url: &str) -> Result<(), Error> {
    let response = reqwest::get(url).await?;

    if response.status().is_success() {
        let body = response.text().await?;
        println!("{}", body);
    } else {
        println!("Error: {}", response.status());
    }
    
    Ok(())
}

fn choose_filters(current_filters: &Vec<&'static str>) -> Vec<&'static str> {
    println!("Choose filters:");
    println!("Filters can be chained together.");
    println!("0. All");
    println!("1. Name");
    println!("2. Currencies");
    println!("3. Languages");
    println!("4. Borders");

    let mut choice = String::new();

    io::stdin()
        .read_line(&mut choice)
        .expect("Failed to read line");

    match choice.trim().parse::<u32>() {
        Ok(0) => vec![],
        Ok(1) => {
            let mut new_filters = current_filters.clone();
            new_filters.push("name");
            new_filters
        }
        Ok(2) => {
            let mut new_filters = current_filters.clone();
            new_filters.push("currencies");
            new_filters
        }
        Ok(3) => {
            let mut new_filters = current_filters.clone();
            new_filters.push("languages");
            new_filters
        }
        Ok(4) => {
            let mut new_filters = current_filters.clone();
            new_filters.push("borders");
            new_filters
        }
        _ => {
            println!("Invalid filter choice. Defaulting to current filters.");
            current_filters.clone()
        }
    }

}
